//
//  Channel.swift
//  tvOSTZ
//
//  Created by Александр Правосудов on 24.07.2020.
//  Copyright © 2020 Александр Правосудов. All rights reserved.
//

struct Channel: Decodable {
    
    // MARK: - Property list
    
    let id: Int
    let orderNumber: Int
    let name: String
    var programs = [Program]()
    
    // MARK: - Initialization
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        id = try container.decode(Int.self, forKey: .id)
        orderNumber = try container.decode(Int.self, forKey: .orderNumber)
        name = try container.decode(String.self, forKey: .name)
    }
    
    // MARK: - CodingKeys
    
    private enum CodingKeys: String, CodingKey {
        case id
        case orderNumber = "orderNum"
        case name = "CallSign"
    }
}
