//
//  Program.swift
//  tvOSTZ
//
//  Created by Александр Правосудов on 24.07.2020.
//  Copyright © 2020 Александр Правосудов. All rights reserved.
//

import Foundation

struct Program: Decodable {
    
    // MARK: - Property list
    
    let id: Int
    let channelID: Int
    let startDate: Date
    let duration: Int
    let name: String
    
    // MARK: - Initialization
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let recentAirTimeContainer = try container.nestedContainer(keyedBy: RecentAirTimeCodingKeys.self,
                                                                   forKey: .recentAirTime)
        
        id = try recentAirTimeContainer.decode(Int.self, forKey: .id)
        channelID = try recentAirTimeContainer.decode(Int.self, forKey: .channelID)
        
        startDate = try container.decode(Date.self, forKey: .startDate)
        duration = try container.decode(Int.self, forKey: .duration)
        name = try container.decode(String.self, forKey: .name)
    }
    
    // MARK: - CodingKeys
    
    private enum CodingKeys: String, CodingKey {
        case startDate = "startTime"
        case duration = "length"
        case name
        case recentAirTime
    }
    
    private enum RecentAirTimeCodingKeys: String, CodingKey {
        case id
        case channelID
    }
}
