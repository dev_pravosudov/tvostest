//
//  TVGuideViewOutput.swift
//  tvOSTZ
//
//  Created by Александр Правосудов on 27.07.2020.
//  Copyright © 2020 Александр Правосудов. All rights reserved.
//

import Foundation
import CoreGraphics

protocol TVGuideViewOutput: ProgramsLayoutDelegate {
    
    func viewDidLoad()
    
    func dateButtonPressed()
    
    func timePeriodsCount() -> Int
    
    func numberOfSections(for collectionViewType: CollectionViewType) -> Int
    func numberOfItems(in section: Int, for collectionViewType: CollectionViewType) -> Int
    func presenterForCell(at indexPath: IndexPath, for collectionViewType: CollectionViewType) -> CellOutputProtocol?
}
