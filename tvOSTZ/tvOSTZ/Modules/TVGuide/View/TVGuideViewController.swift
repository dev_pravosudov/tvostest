//
//  TVGuideViewController.swift
//  tvOSTZ
//
//  Created by Александр Правосудов on 27.07.2020.
//  Copyright © 2020 Александр Правосудов. All rights reserved.
//

import UIKit

final class TVGuideViewController: BaseViewController, TVGuideViewInput, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    // MARK: - Property list

    var presenter: TVGuideViewOutput!
    var programsLayout: ProgramsLayout!
    
    @IBOutlet private weak var dateButton: UIButton!
    @IBOutlet private weak var timeCollectionView: UICollectionView!
    @IBOutlet private weak var channelsCollectionView: UICollectionView!
    @IBOutlet private weak var programsCollectionView: UICollectionView!
    @IBOutlet private weak var loaderView: UIView!

    // MARK: - Overrides

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
        
        setupCollectionViews()
    }
    
    override var preferredFocusEnvironments: [UIFocusEnvironment] {
        return [dateButton]
    }
        
    // MARK: - TVGuideViewInput
    
    func reloadChannelsCollectionView() {
        channelsCollectionView.reloadData()
    }
    
    func reloadProgramsCollectionView() {
        programsCollectionView.reloadData()
    }
    
    func showLoader() {
        loaderView.show()
    }
    
    func hideLoader() {
        loaderView.hide()
    }

    // MARK: - Private methods
    
    private func setupCollectionViews() {
        timeCollectionView.registerWithNib(cellType: TimeCollectionCell.self)
        channelsCollectionView.registerWithNib(cellType: ChannelCollectionCell.self)
        programsCollectionView.registerWithNib(cellType: ProgramCollectionCell.self)
        
        programsCollectionView.collectionViewLayout = programsLayout
    }
    
    private func collectionViewType(_ collectionView: UICollectionView) -> CollectionViewType {
        switch collectionView {
        case timeCollectionView: return .time
        case channelsCollectionView: return .channel
        default: return .program
        }
    }

    // MARK: - Actions
    
    @IBAction private func dateButtonPressed(_ sender: UIButton) {
        presenter.dateButtonPressed()
    }
    
    // MARK: - UIScrollViewDelegate
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let timeOffset = timeCollectionView.contentOffset
        let channelsOffset = channelsCollectionView.contentOffset
        let programsOffset = programsCollectionView.contentOffset
        
        switch scrollView {
        case timeCollectionView:
            programsCollectionView.contentOffset = CGPoint(x: timeOffset.x, y: programsOffset.y)
        case channelsCollectionView:
            programsCollectionView.contentOffset = CGPoint(x: programsOffset.x, y: channelsOffset.y)
        case programsCollectionView:
            timeCollectionView.contentOffset = CGPoint(x: programsOffset.x, y: timeOffset.y)
            channelsCollectionView.contentOffset = CGPoint(x: channelsOffset.x, y: programsOffset.y)
        default: return
        }
    }
    
    // MARK: - UICollectionViewDelegateFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch collectionView {
        case timeCollectionView: return CGSize(width: 300, height: 60)
        case channelsCollectionView: return CGSize(width: 200, height: 100)
        default: return .zero
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    // MARK: - UICollectionViewDelegate
    
    // MARK: - UICollectionViewDataSource
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        let collectionType = collectionViewType(collectionView)
        return presenter.numberOfSections(for: collectionType)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let collectionType = collectionViewType(collectionView)
        return presenter.numberOfItems(in: section, for: collectionType)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let collectionType = collectionViewType(collectionView)
        
        switch presenter.presenterForCell(at: indexPath, for: collectionType) {
            
        case let presenter as TimeCollectionCellOutput:
            let cell = collectionView.dequeue(cell: TimeCollectionCell.self, for: indexPath)
            cell.presenter = presenter
            return cell
            
        case let presenter as ChannelCollectionCellOutput:
            let cell = collectionView.dequeue(cell: ChannelCollectionCell.self, for: indexPath)
            cell.presenter = presenter
            return cell
            
        case let presenter as ProgramCollectionCellOutput:
            let cell = collectionView.dequeue(cell: ProgramCollectionCell.self, for: indexPath)
            cell.presenter = presenter
            return cell

        default: return UICollectionViewCell()
        }
    }
}
