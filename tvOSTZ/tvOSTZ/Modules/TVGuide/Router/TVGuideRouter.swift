//
//  TVGuideRouter.swift
//  tvOSTZ
//
//  Created by Александр Правосудов on 27.07.2020.
//  Copyright © 2020 Александр Правосудов. All rights reserved.
//

import UIKit

final class TVGuideRouter: TVGuideRouterInput {

    // MARK: - Property list

    weak var transitionHandler: UIViewController!
    
    // MARK: - TVGuideRouterInput
    
    func showDatePicker() {
        let datePickerVC = UIStoryboard.screen(DatePickerViewController.self)
        transitionHandler.present(datePickerVC, animated: true)
    }
}
