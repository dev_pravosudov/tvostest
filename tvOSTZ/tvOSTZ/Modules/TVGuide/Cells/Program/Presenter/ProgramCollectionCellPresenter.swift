//
//  ProgramCollectionCellPresenter.swift
//  tvOSTZ
//
//  Created by Александр Правосудов on 30.07.2020.
//  Copyright © 2020 Александр Правосудов. All rights reserved.
//

import Foundation

final class ProgramCollectionCellPresenter: ProgramCollectionCellOutput {
    
    // MARK: - Property list
    
    weak var cell: ProgramCollectionCellInput?
    
    var programName: String { program.name }
    var programStartTime: String { program.startDate.timeString }
    var programStartDate: Date { program.startDate }
    var programDuration: Int { program.duration }
    
    private let program: Program
    
    // MARK: - Initialization
    
    init(program: Program) {
        self.program = program
    }
    
    // MARK: - ProgramCollectionCellOutput
}
