//
//  ProgramCollectionCellOutput.swift
//  tvOSTZ
//
//  Created by Александр Правосудов on 30.07.2020.
//  Copyright © 2020 Александр Правосудов. All rights reserved.
//

import Foundation

protocol ProgramCollectionCellOutput: CellOutputProtocol {
 
    var cell: ProgramCollectionCellInput? { get set }
    
    var programName: String { get }
    var programStartTime: String { get }
}
