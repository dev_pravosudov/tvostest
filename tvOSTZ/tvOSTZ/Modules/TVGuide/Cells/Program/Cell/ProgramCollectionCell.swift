//
//  ProgramCollectionCell.swift
//  tvOSTZ
//
//  Created by Александр Правосудов on 30.07.2020.
//  Copyright © 2020 Александр Правосудов. All rights reserved.
//

import UIKit

final class ProgramCollectionCell: UICollectionViewCell, ProgramCollectionCellInput, SelectionItemProtocol {
    
    // MARK: - Property list
    
    var presenter: ProgramCollectionCellOutput? {
        willSet {
            presenter?.cell = nil
        }
        didSet {
            presenter?.cell = self
            configure()
        }
    }
    
    @IBOutlet private weak var selectionBackgroud: UIView!
    @IBOutlet private weak var programNameLabel: UILabel!
    @IBOutlet private weak var programStartTimeLabel: UILabel!

    // MARK: - ProgramCollectionCellInput
    
    // MARK: - SelectionItemProtocol
    
    func select() {
        selectionBackgroud.backgroundColor = #colorLiteral(red: 0, green: 0.946221292, blue: 0.9373136163, alpha: 1)
        programStartTimeLabel.show()
    }
    
    func deselect() {
        selectionBackgroud.backgroundColor = #colorLiteral(red: 0.01864761114, green: 0.1024372205, blue: 0.1743693948, alpha: 1)
        programStartTimeLabel.hide()
    }
    
    // MARK: - Private methods
    
    private func configure() {
        guard let presenter = presenter else { return }
        
        programNameLabel.text = presenter.programName
        programStartTimeLabel.text = presenter.programStartTime
    }
}
