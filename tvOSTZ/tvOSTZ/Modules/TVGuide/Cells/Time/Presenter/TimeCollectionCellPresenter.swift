//
//  TimeCollectionCellPresenter.swift
//  tvOSTZ
//
//  Created by Александр Правосудов on 28.07.2020.
//  Copyright © 2020 Александр Правосудов. All rights reserved.
//

final class TimeCollectionCellPresenter: TimeCollectionCellOutput {
    
    // MARK: - Property list
    
    weak var cell: TimeCollectionCellInput?
    
    let timeValue: String
    
    // MARK: - Initialization
    
    init(timeValue: String) {
        self.timeValue = timeValue
    }
    
    // MARK: - TimeCollectionCellOutput
}
