//
//  TimeCollectionCellOutput.swift
//  tvOSTZ
//
//  Created by Александр Правосудов on 28.07.2020.
//  Copyright © 2020 Александр Правосудов. All rights reserved.
//

protocol TimeCollectionCellOutput: CellOutputProtocol {
    
    var cell: TimeCollectionCellInput? { get set }
    
    var timeValue: String { get }
}
