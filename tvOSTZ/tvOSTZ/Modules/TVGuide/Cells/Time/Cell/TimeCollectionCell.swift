//
//  TimeCollectionCell.swift
//  tvOSTZ
//
//  Created by Александр Правосудов on 28.07.2020.
//  Copyright © 2020 Александр Правосудов. All rights reserved.
//

import UIKit

final class TimeCollectionCell: UICollectionViewCell, TimeCollectionCellInput, SelectionItemProtocol {
    
    // MARK: - Property list
    
    var presenter: TimeCollectionCellOutput? {
        willSet {
            presenter?.cell = nil
        }
        didSet {
            presenter?.cell = self
            configure()
        }
    }
    
    @IBOutlet private weak var timeLabel: UILabel!
    
    // MARK: - TimeCollectionCellInput
    
    // MARK: - SelectionItemProtocol
    
    func select() {
        backgroundColor = #colorLiteral(red: 0, green: 0.946221292, blue: 0.9373136163, alpha: 1)
    }
    
    func deselect() {
        backgroundColor = .clear
    }
    
    // MARK: - Private methods
    
    private func configure() {
        timeLabel.text = presenter?.timeValue
    }
}
