//
//  ChannelCollectionCellPresenter.swift
//  tvOSTZ
//
//  Created by Александр Правосудов on 28.07.2020.
//  Copyright © 2020 Александр Правосудов. All rights reserved.
//

final class ChannelCollectionCellPresenter: ChannelCollectionCellOutput {
    
    // MARK: - Property list
    
    weak var cell: ChannelCollectionCellInput?
    
    var channelNumber: String { String(channel.orderNumber) }
    var channelName: String { channel.name }
    
    private let channel: Channel
    
    // MARK: - Initialization
    
    init(channel: Channel) {
        self.channel = channel
    }
    
    // MARK: - ChannelCollectionCellOutput
}
