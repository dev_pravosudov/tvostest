//
//  ChannelCollectionCellOutput.swift
//  tvOSTZ
//
//  Created by Александр Правосудов on 28.07.2020.
//  Copyright © 2020 Александр Правосудов. All rights reserved.
//

protocol ChannelCollectionCellOutput: CellOutputProtocol {
    
    var cell: ChannelCollectionCellInput? { get set }
    
    var channelNumber: String { get }
    var channelName: String { get }
}
