//
//  ChannelCollectionCell.swift
//  tvOSTZ
//
//  Created by Александр Правосудов on 28.07.2020.
//  Copyright © 2020 Александр Правосудов. All rights reserved.
//

import UIKit

final class ChannelCollectionCell: UICollectionViewCell, ChannelCollectionCellInput, SelectionItemProtocol {
    
    // MARK: - Property list
    
    var presenter: ChannelCollectionCellOutput? {
        willSet {
            presenter?.cell = nil
        }
        didSet {
            presenter?.cell = self
            configure()
        }
    }
    
    @IBOutlet private weak var channelNumberLabel: UILabel!
    @IBOutlet private weak var channelNameLabel: UILabel!

    // MARK: - ChannelCollectionCellInput
    
    // MARK: - SelectionItemProtocol
    
    func select() {
        backgroundColor = #colorLiteral(red: 0, green: 0.946221292, blue: 0.9373136163, alpha: 1)
    }
    
    func deselect() {
        backgroundColor = .clear
    }
    
    // MARK: - Private methods
    
    private func configure() {
        guard let presenter = presenter else { return }
        
        channelNumberLabel.text = presenter.channelNumber
        channelNameLabel.text = presenter.channelName
    }
}
