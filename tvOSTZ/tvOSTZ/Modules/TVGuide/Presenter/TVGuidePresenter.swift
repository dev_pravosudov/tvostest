//
//  TVGuidePresenter.swift
//  tvOSTZ
//
//  Created by Александр Правосудов on 27.07.2020.
//  Copyright © 2020 Александр Правосудов. All rights reserved.
//

import Foundation
import CoreGraphics

final class TVGuidePresenter: TVGuideViewOutput, TVGuideInteractorOutput {

    // MARK: - Property list
    
    weak var view: TVGuideViewInput!
    var interactor: TVGuideInteractorInput!
    var router: TVGuideRouterInput!
    
    private let timePresentersArray = DataProvider.timePeriods.map(TimeCollectionCellPresenter.init)

    private var channelPresentersArray = [ChannelCollectionCellPresenter]()
    private var programPresentersArray = [[ProgramCollectionCellPresenter]]()

    // MARK: - TVGuideViewOutput
    
    func viewDidLoad() {
        view.showLoader()
        interactor.getChannels()
    }
    
    func dateButtonPressed() {
        router.showDatePicker()
    }
    
    func timePeriodsCount() -> Int {
        return timePresentersArray.count
    }
    
    func programContentSize() -> CGSize {
        let width = numberOfItems(in: 0, for: .time) * 300
        let height = numberOfSections(for: .program) * 100
        return CGSize(width: width, height: height)
    }
    
    func programItemSize(for indexPath: IndexPath) -> CGSize {
        guard let program = programPresentersArray[safe: indexPath.section]?[safe: indexPath.row] else { return .zero }
        
        let startMinutes = 3 * 60
        let programStartMinutes = program.programStartDate.timeInMinutes
        let timeDifference = min(0, programStartMinutes - startMinutes)
        
        let width = indexPath.row == 0 ? (program.programDuration + timeDifference) * 10 : (program.programDuration * 10)
        return CGSize(width: width, height: 100)
    }
    
    func programInset(for section: Int) -> CGFloat {
        guard let firstProgramDate = programPresentersArray[section].first?.programStartDate else { return 0 }
        
        let startMinutes = 3 * 60
        let timeDifference = max(0, firstProgramDate.timeInMinutes - startMinutes)
        let sectionInset = timeDifference * 10
        return CGFloat(sectionInset)
    }
        
    func numberOfSections(for collectionViewType: CollectionViewType) -> Int {
        switch collectionViewType {
        case .program: return max(1, programPresentersArray.count)
        case .time, .channel: return 1
        }
    }
    
    func numberOfProgramSections() -> Int {
        return programPresentersArray.count
    }
    
    func numberOfProgramItems(in section: Int) -> Int {
        return programPresentersArray[safe: section]?.count ?? 0
    }
    
    func numberOfItems(in section: Int, for collectionViewType: CollectionViewType) -> Int {
        switch collectionViewType {
        case .time: return timePresentersArray.count
        case .channel: return channelPresentersArray.count
        case .program: return numberOfProgramItems(in: section)
        }
    }
    
    func presenterForCell(at indexPath: IndexPath, for collectionViewType: CollectionViewType) -> CellOutputProtocol? {
        switch collectionViewType {
        case .time: return timePresentersArray[indexPath.row]
        case .channel: return channelPresentersArray[indexPath.row]
        case .program: return programPresentersArray[indexPath.section][indexPath.row]
        }
    }
    
    // MARK: - TVGuideInteractorOutput
    
    func successGetChannels(_ channels: [Channel]) {
        channelPresentersArray = channels.map(ChannelCollectionCellPresenter.init)
        
        programPresentersArray = channels.map { $0.programs }
            .reduce([[ProgramCollectionCellPresenter]](), {( result, programs) in
                var result = result
                let programPresentersArray = programs.map { ProgramCollectionCellPresenter(program: $0) }
                result.append(programPresentersArray)
                return result
        })
        view.reloadChannelsCollectionView()
        view.reloadProgramsCollectionView()
        view.hideLoader()
    }
    
    func failureGetChannels(_ errors: [Error]?) {
        view.hideLoader()
        // TODO: - Тут должна быть обработка ошибок
    }

    // MARK: - Private methods
}
