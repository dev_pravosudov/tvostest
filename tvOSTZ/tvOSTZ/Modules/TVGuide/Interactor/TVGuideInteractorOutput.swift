//
//  TVGuideInteractorOutput.swift
//  tvOSTZ
//
//  Created by Александр Правосудов on 27.07.2020.
//  Copyright © 2020 Александр Правосудов. All rights reserved.
//

protocol TVGuideInteractorOutput: class {
    
    func successGetChannels(_ channels: [Channel])
    func failureGetChannels(_ errors: [Error]?)    
}
