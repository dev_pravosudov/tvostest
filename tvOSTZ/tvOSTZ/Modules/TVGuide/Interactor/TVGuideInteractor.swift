//
//  TVGuideInteractor.swift
//  tvOSTZ
//
//  Created by Александр Правосудов on 27.07.2020.
//  Copyright © 2020 Александр Правосудов. All rights reserved.
//

import Foundation

private typealias ChannelId = Int

final class TVGuideInteractor: TVGuideInteractorInput {
    
    // MARK: - Property list
    
    weak var presenter: TVGuideInteractorOutput!
    var apiManager: ApiManagerTV!
    var dispatchGroup: DispatchGroup!
    
    private var channels: [Channel]?
    private var programs: [Program]?
    private var errors: [Error]?
    
    // MARK: -  TVGuideInteractorInput
    
    func getChannels() {
        dispatchGroup.enter()
        apiManager.getChannels(success: successGetChannels,
                               failure: requestFailure)
        
        dispatchGroup.enter()
        apiManager.getPrograms(success: successGetPrograms,
                               failure: requestFailure)
        
        dispatchGroup.notify(qos: .userInitiated,
                             queue: .main,
                             execute: notifyChannelsAndProgramsRequestFinished)
    }

    // MARK: - Blocks realization
    
    private lazy var successGetChannels: ChannelsBlock = { [weak self] channels in
        guard let self = self else { return }
        
        self.channels = channels
        self.dispatchGroup.leave()
    }
    
    private lazy var successGetPrograms: ProgramsBlock = { [weak self] programs in
        guard let self = self else { return }
        
        self.programs = programs
        self.dispatchGroup.leave()
    }
    
    private lazy var requestFailure: ErrorBlock = { [weak self] error in
        guard let self = self else { return }
        
        self.errors == nil ? (self.errors = [error]) : (self.errors?.append(error))
        self.dispatchGroup.leave()
    }
    
    private lazy var notifyChannelsAndProgramsRequestFinished: EmptyBlock = { [weak self] in
        guard let self = self else { return }
            
        guard let channels = self.channels, let programs = self.programs else {
            self.presenter.failureGetChannels(self.errors)
            return
        }
        
        let programsMap = self.programsMap(from: programs)
        let fullChannels = self.fullChannels(channels, with: programsMap)
        
        self.presenter.successGetChannels(fullChannels)
        self.clearStoredData()
    }
    
    // MARK: - Private methods
    
    private func programsMap(from programs: [Program]) -> [ChannelId: [Program]] {
        var programsMap = [ChannelId: [Program]]()
        
        for program in programs {
            if var programArray = programsMap[program.channelID] {
                programArray.append(program)
                programsMap[program.channelID] = programArray
            } else {
                let programArray = [program]
                programsMap[program.channelID] = programArray
            }
        }
        
        return programsMap
    }
    
    private func fullChannels(_ channels: [Channel], with programsMap: [ChannelId: [Program]]) -> [Channel] {
        var newFullChannels = [Channel]()
        for channel in channels {
            if let programsArray = programsMap[channel.id] {
                var fullChannelInfo = channel
                fullChannelInfo.programs = programsArray
                newFullChannels.append(fullChannelInfo)
            }
        }
        
        return newFullChannels
    }
    
    private func clearStoredData() {
        channels = nil
        programs = nil
        errors = nil
    }
}
