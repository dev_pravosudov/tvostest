//
//  ProgramLayoutDelegate.swift
//  tvOSTZ
//
//  Created by Александр Правосудов on 02.08.2020.
//  Copyright © 2020 Александр Правосудов. All rights reserved.
//

import Foundation
import CoreGraphics

protocol ProgramsLayoutDelegate: class {
    
    func programContentSize() -> CGSize
    func programInset(for section: Int) -> CGFloat
    func programItemSize(for indexPath: IndexPath) -> CGSize
    func numberOfProgramSections() -> Int
    func numberOfProgramItems(in section: Int) -> Int
}
