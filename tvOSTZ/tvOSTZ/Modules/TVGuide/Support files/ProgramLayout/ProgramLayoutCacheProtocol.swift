//
//  ProgramLayoutCacheProtocol.swift
//  tvOSTZ
//
//  Created by Александр Правосудов on 02.08.2020.
//  Copyright © 2020 Александр Правосудов. All rights reserved.
//

import UIKit

protocol ProgramLayoutCacheProtocol {
 
    var programAttributes: [IndexPath: UICollectionViewLayoutAttributes] { get set }
    
    func programAttributes(in rect: CGRect) -> [UICollectionViewLayoutAttributes]
}
