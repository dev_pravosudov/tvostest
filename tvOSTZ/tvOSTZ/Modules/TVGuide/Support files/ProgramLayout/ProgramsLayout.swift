//
//  ProgramsLayout.swift
//  tvOSTZ
//
//  Created by Александр Правосудов on 30.07.2020.
//  Copyright © 2020 Александр Правосудов. All rights reserved.
//

import UIKit

final class ProgramsLayout : UICollectionViewFlowLayout {
    
    // MARK - Property list
        
    var cache: ProgramLayoutCacheProtocol!
    weak var delegate: ProgramsLayoutDelegate?
        
    // MARK: - Overrides
    
    override func prepare() {
        guard let delegate = delegate, cache.programAttributes.count == 0 else { return }
        
        for section in 0..<delegate.numberOfProgramSections() {
            for row in 0..<delegate.numberOfProgramItems(in: section) {
                
                let indexPath = IndexPath(row: row, section: section)
                
                let attribute = UICollectionViewLayoutAttributes(forCellWith: indexPath)
                let size = delegate.programItemSize(for: indexPath)
                
                let x = previosSize(before: indexPath)
                let y = CGFloat(section) * size.height
                
                attribute.frame = CGRect(x: x, y: y, width: size.width, height: size.height)
                
                cache.programAttributes[indexPath] = attribute
            }
        }
    }
    
    override var collectionViewContentSize: CGSize {
        guard let delegate = delegate else { return .zero }
        
        return delegate.programContentSize()
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return cache.programAttributes[indexPath]
    }

    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        return cache.programAttributes(in: rect)
    }
        
    // MARK: - Private methods
    
    private func previosSize(before indexPath: IndexPath) -> CGFloat {
        guard let delegate = delegate else { return 0 }
        
        let section = indexPath.section
        let sectionInset = delegate.programInset(for: section)
        
        if indexPath.row == 0 { return sectionInset }
        
        let previousRow = max(0, indexPath.row - 1)
        
        var newXPos = sectionInset
        for row in 0...previousRow {
            let itemSize = delegate.programItemSize(for: IndexPath(row: row, section: section))
            newXPos += itemSize.width
        }
        
        return newXPos
    }
}
