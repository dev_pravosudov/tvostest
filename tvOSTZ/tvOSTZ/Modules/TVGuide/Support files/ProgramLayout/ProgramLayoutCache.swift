
//  ProgramLayoutCache.swift
//  tvOSTZ
//
//  Created by Александр Правосудов on 02.08.2020.
//  Copyright © 2020 Александр Правосудов. All rights reserved.
//

import UIKit

final class ProgramLayoutCache: ProgramLayoutCacheProtocol {
    
    // MARK: - Property list
    
    var programAttributes = [IndexPath: UICollectionViewLayoutAttributes]()
    
    // MARK: - Public methods
    
    func programAttributes(in rect: CGRect) -> [UICollectionViewLayoutAttributes] {
        return programAttributes.map { $1 }.filter { rect.intersects($0.frame) }
    }
}
