//
//  CollectionViewType.swift
//  tvOSTZ
//
//  Created by Александр Правосудов on 29.07.2020.
//  Copyright © 2020 Александр Правосудов. All rights reserved.
//

enum CollectionViewType {
    case time
    case channel
    case program
}
