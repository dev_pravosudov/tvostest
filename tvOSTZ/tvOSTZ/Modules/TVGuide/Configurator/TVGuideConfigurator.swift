//
//  TVGuideConfigurator.swift
//  tvOSTZ
//
//  Created by Александр Правосудов on 27.07.2020.
//  Copyright © 2020 Александр Правосудов. All rights reserved.
//

import Foundation

final class TVGuideModuleConfigurator {

    // MARK: - Public methods

    static func configureModule(with viewController: TVGuideViewController) {
        let router = TVGuideRouter()
        router.transitionHandler = viewController
        
        let interactor = TVGuideInteractor()
        interactor.apiManager = ApiManager.shared
        interactor.dispatchGroup = DispatchGroup()
        
        let presenter = TVGuidePresenter()
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.presenter = presenter
        
        let programsLayout = ProgramsLayout()
        programsLayout.cache = ProgramLayoutCache()
        programsLayout.delegate = presenter
        
        viewController.presenter = presenter
        viewController.programsLayout = programsLayout
    }
}
