//
//  TVGuideInitializer.swift
//  tvOSTZ
//
//  Created by Александр Правосудов on 27.07.2020.
//  Copyright © 2020 Александр Правосудов. All rights reserved.
//

import Foundation
   
final class TVGuideModuleInitializer: NSObject {

    // MARK: - Property list

    @IBOutlet private weak var tVGuideViewController: TVGuideViewController!
    
    // MARK: - Overrides

    override func awakeFromNib() {
        TVGuideModuleConfigurator.configureModule(with: tVGuideViewController)
    }
}
