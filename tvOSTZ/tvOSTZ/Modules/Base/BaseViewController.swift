//
//  BaseViewController.swift
//  tvOSTZ
//
//  Created by Александр Правосудов on 04.08.2020.
//  Copyright © 2020 Александр Правосудов. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    // MARK: - Overrides

    override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        super.didUpdateFocus(in: context, with: coordinator)
        
        if let nextView = context.nextFocusedView as? SelectionItemProtocol {
            nextView.select()
        }
            
        if let previosView = context.previouslyFocusedView as? SelectionItemProtocol {
            previosView.deselect()
        }
    }
}
