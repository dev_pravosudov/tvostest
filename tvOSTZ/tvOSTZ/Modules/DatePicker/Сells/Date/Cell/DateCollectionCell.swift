//
//  DateCollectionCell.swift
//  tvOSTZ
//
//  Created by Александр Правосудов on 03.08.2020.
//  Copyright © 2020 Александр Правосудов. All rights reserved.
//

import UIKit

final class DateCollectionCell: UICollectionViewCell, DateCollectionCellInput, SelectionItemProtocol {
    
    // MARK: - Property list
    
    var presenter: DateCollectionCellOutput? {
        willSet {
            presenter?.cell = nil
        }
        didSet {
            presenter?.cell = self
            configure()
        }
    }
    
    @IBOutlet private weak var dateLabel: UILabel!

    // MARK: - ProgramCollectionCellInput
    
    // MARK: - SelectionItemProtocol
    
    func select() {
        backgroundColor = #colorLiteral(red: 0, green: 0.946221292, blue: 0.9373136163, alpha: 1)
    }
    
    func deselect() {
        backgroundColor = #colorLiteral(red: 0.01960966736, green: 0.1024211124, blue: 0.1743649542, alpha: 1)
    }
    
    // MARK: - Private methods
    
    private func configure() {
        guard let presenter = presenter else { return }
        
        dateLabel.text = presenter.dateValue
    }
}
