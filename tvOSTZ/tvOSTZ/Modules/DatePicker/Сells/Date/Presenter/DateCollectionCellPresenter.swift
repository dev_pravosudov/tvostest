//
//  DateCollectionCellPresenter.swift
//  tvOSTZ
//
//  Created by Александр Правосудов on 03.08.2020.
//  Copyright © 2020 Александр Правосудов. All rights reserved.
//

final class DateCollectionCellPresenter: DateCollectionCellOutput {
    
    // MARK: - Property list
    
    weak var cell: DateCollectionCellInput?
    
    let dateValue: String
    
    // MARK: - Initialization
    
    init(dateValue: String) {
        self.dateValue = dateValue
    }
    
    // MARK: - DateCollectionCellOutput
}
