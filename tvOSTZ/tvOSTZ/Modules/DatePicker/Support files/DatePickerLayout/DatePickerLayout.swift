//
//  DatePickerLayout.swift
//  tvOSTZ
//
//  Created by Александр Правосудов on 04.08.2020.
//  Copyright © 2020 Александр Правосудов. All rights reserved.
//

import UIKit

private typealias ScrollingBehavior = UICollectionLayoutSectionOrthogonalScrollingBehavior

final class DatePickerLayout {
    
    // MARK: - Public methods
        
    static func create() -> UICollectionViewCompositionalLayout {
        return UICollectionViewCompositionalLayout(sectionProvider: sectionProvider,
                                                         configuration: configurator)
    }
    
    // MARK: - Private methods
    
    private static var sectionProvider: UICollectionViewCompositionalLayoutSectionProvider {
        let sectionProvider: UICollectionViewCompositionalLayoutSectionProvider = { (sectionIndex, layoutEnvironment) in
            guard let section = DatePickerSection(rawValue: sectionIndex) else { return nil }
            
            switch section {
            case .year: return self.yearSection
            case .day: return self.daysSection
            case .month: return self.monthSection
            }
        }
        
        return sectionProvider
    }
    
    private static var configurator: UICollectionViewCompositionalLayoutConfiguration {
        let configurator = UICollectionViewCompositionalLayoutConfiguration()
        configurator.interSectionSpacing = 10
        return configurator
    }
    
    private static var yearSection: NSCollectionLayoutSection {
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0),
                                              heightDimension: .fractionalHeight(1.0))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0),
                                               heightDimension: .fractionalHeight(1.0))
        let group = NSCollectionLayoutGroup.vertical(layoutSize: groupSize,
                                                     subitem: item,
                                                     count: 3)
        group.interItemSpacing = .fixed(2)
        
        let rootGroupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0/3.5),
                                                   heightDimension: .fractionalHeight(1.0/5.0))
        let rootGroup = NSCollectionLayoutGroup.horizontal(layoutSize: rootGroupSize,
                                                           subitems: [group])
        
        return section(group: rootGroup, scrollingBehavior: .continuous)
    }
    
    private static var monthSection: NSCollectionLayoutSection {
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0),
                                              heightDimension: .fractionalHeight(1.0))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(0.15),
                                               heightDimension: .fractionalHeight(1.0/5.0))
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize,
                                                       subitems: [item])
        
        return section(group: group, scrollingBehavior: .continuous)
    }
    
    private static var daysSection: NSCollectionLayoutSection {
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0),
                                              heightDimension: .fractionalHeight(1.0))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0),
                                               heightDimension: .fractionalWidth(0.07))
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize,
                                                       subitem: item,
                                                       count: 10)
        group.interItemSpacing = .fixed(10)

        return section(group: group)
    }
        
    private static func section(group: NSCollectionLayoutGroup,
                                scrollingBehavior: ScrollingBehavior = .none) -> NSCollectionLayoutSection {
        let section = NSCollectionLayoutSection(group: group)
        section.interGroupSpacing = 10
        section.orthogonalScrollingBehavior = scrollingBehavior
        section.contentInsets = .init(top: 10, leading: 10, bottom: 10, trailing: 10)
        section.boundarySupplementaryItems = [header]
        
        return section
    }
    
    private static var header: NSCollectionLayoutBoundarySupplementaryItem {
        let headerSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0),
                                                heightDimension: .absolute(50.0))
        return NSCollectionLayoutBoundarySupplementaryItem(layoutSize: headerSize,
                                                           elementKind: DateHeaderView.className,
                                                           alignment: .top)
    }
}
