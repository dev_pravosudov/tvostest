//
//  DatePickerSection.swift
//  tvOSTZ
//
//  Created by Александр Правосудов on 04.08.2020.
//  Copyright © 2020 Александр Правосудов. All rights reserved.
//

enum DatePickerSection: Int {
    case year
    case month
    case day
}

