//
//  DatePickerPresenter.swift
//  tvOSTZ
//
//  Created by Александр Правосудов on 03.08.2020.
//  Copyright © 2020 Александр Правосудов. All rights reserved.
//

import Foundation

final class DatePickerPresenter: DatePickerModuleInput, DatePickerViewOutput, DatePickerInteractorOutput {
    
    // MARK: - Property list
    
    weak var view: DatePickerViewInput!
    var interactor: DatePickerInteractorInput!
    var router: DatePickerRouterInput!
    
    var yearsRange: ClosedRange<Int> { 1...numberOfItems(in: .year) }
    var monthsRange: ClosedRange<Int> { (yearsRange.upperBound + 1)...(yearsRange.upperBound + numberOfItems(in: .month)) }
    var daysRange: ClosedRange<Int> { (monthsRange.upperBound + 1)...(monthsRange.upperBound + numberOfItems(in: .day)) }
    
    private let headersArray = DataProvider.dateHeaders
    private let yearsArray = DataProvider.years.map(DateCollectionCellPresenter.init)
    private let monthsArray = DataProvider.months.map(DateCollectionCellPresenter.init)
    private let daysArray = DataProvider.days.map(DateCollectionCellPresenter.init)
    
    // MARK: - DatePickerViewOutput
    
    func headerTitle(for indexPath: IndexPath) -> String? {
        guard let section = DatePickerSection(rawValue: indexPath.section) else { return nil }

        return headersArray[safe: section.rawValue]
    }
    
    func numberOfItems(in section: DatePickerSection) -> Int {
        switch section {
        case .year: return yearsArray.count
        case .month: return monthsArray.count
        case .day: return daysArray.count
        }
    }
    
    func presenterForCell(at indexPath: IndexPath) -> DateCollectionCellOutput? {
        guard let section = DatePickerSection(rawValue: indexPath.section) else { return nil }
        
        switch section {
        case .year: return yearsArray[indexPath.row]
        case .month: return monthsArray[indexPath.row]
        case .day: return daysArray[indexPath.row]
        }
    }
}
