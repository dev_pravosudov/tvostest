//
//  DatePickerViewOutput.swift
//  tvOSTZ
//
//  Created by Александр Правосудов on 03.08.2020.
//  Copyright © 2020 Александр Правосудов. All rights reserved.
//

import Foundation

protocol DatePickerViewOutput {
    
    var yearsRange: ClosedRange<Int> { get }
    var monthsRange: ClosedRange<Int> { get }
    var daysRange: ClosedRange<Int> { get }

    func headerTitle(for indexPath: IndexPath) -> String?
    func numberOfItems(in section: DatePickerSection) -> Int
    func presenterForCell(at indexPath: IndexPath) -> DateCollectionCellOutput?
}
