//
//  DatePickerViewController.swift
//  tvOSTZ
//
//  Created by Александр Правосудов on 03.08.2020.
//  Copyright © 2020 Александр Правосудов. All rights reserved.
//

import UIKit

final class DatePickerViewController: BaseViewController, DatePickerViewInput, UICollectionViewDelegate {
    
    // MARK: - Property list
    
    var presenter: DatePickerViewOutput!
    
    private lazy var dataSource = createDataSource()
        
    @IBOutlet private weak var collectionView: UICollectionView!
        
    // MARK: - Overrides
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupCollectionView()
        setupHeaderForDataSource()
        applySnashot()
    }
    
    override var preferredFocusEnvironments: [UIFocusEnvironment] {
        return [collectionView]
    }
        
    // MARK: - Private methods
    
    private func createDataSource() -> UICollectionViewDiffableDataSource<DatePickerSection, Int> {
        return UICollectionViewDiffableDataSource<DatePickerSection, Int>(collectionView: collectionView) {
            [weak self] (collectionView, indexPath, identifier) in

            let cell = collectionView.dequeue(cell: DateCollectionCell.self, for: indexPath)
            cell.presenter = self?.presenter.presenterForCell(at: indexPath)
            return cell
        }
    }
    
    private func setupCollectionView() {
        collectionView.registerWithNib(cellType: DateCollectionCell.self)
        collectionView.registerWithNib(viewType: DateHeaderView.self)
        collectionView.collectionViewLayout = DatePickerLayout.create()
    }
        
    private func setupHeaderForDataSource() {
        dataSource.supplementaryViewProvider = { [weak self] (collectionView, kind, indexPath) in
            guard let self = self else { return nil }
            
            let headerView = collectionView.dequeue(view: DateHeaderView.self, for: indexPath)
            let title = self.presenter.headerTitle(for: indexPath)
                
            headerView.setTitle(title)
            return headerView
        }
    }
        
    private func applySnashot() {
        var snapshot = NSDiffableDataSourceSnapshot<DatePickerSection, Int>()
        snapshot.appendSections([.year, .month, .day])
        snapshot.appendItems(Array(presenter.yearsRange), toSection: .year)
        snapshot.appendItems(Array(presenter.monthsRange), toSection: .month)
        snapshot.appendItems(Array(presenter.daysRange), toSection: .day)
        
        dataSource.apply(snapshot, animatingDifferences: false)
    }
}
