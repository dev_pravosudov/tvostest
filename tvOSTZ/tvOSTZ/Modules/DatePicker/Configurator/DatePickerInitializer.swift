//
//  DatePickerInitializer.swift
//  tvOSTZ
//
//  Created by Александр Правосудов on 03.08.2020.
//  Copyright © 2020 Александр Правосудов. All rights reserved.
//

import Foundation
   
final class DatePickerInitializer: NSObject {

    // MARK: - Property list

    @IBOutlet private weak var datePickerViewController: DatePickerViewController!
    
    // MARK: - Overrides

    override func awakeFromNib() {
        DatePickerConfigurator.configureModule(with: datePickerViewController)
    }
}
