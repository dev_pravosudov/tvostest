//
//  DatePickerConfigurator.swift
//  tvOSTZ
//
//  Created by Александр Правосудов on 03.08.2020.
//  Copyright © 2020 Александр Правосудов. All rights reserved.
//

final class DatePickerConfigurator {

    // MARK: - Public methods

    static func configureModule(with viewController: DatePickerViewController) {
        let router = DatePickerRouter()
        router.transitionHandler = viewController
        
        let interactor = DatePickerInteractor()
        
        let presenter = DatePickerPresenter()
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.presenter = presenter
        
        viewController.presenter = presenter
    }
}
