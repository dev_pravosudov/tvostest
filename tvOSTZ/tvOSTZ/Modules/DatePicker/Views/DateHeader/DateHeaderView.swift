//
//  DateHeaderView.swift
//  tvOSTZ
//
//  Created by Александр Правосудов on 04.08.2020.
//  Copyright © 2020 Александр Правосудов. All rights reserved.
//

import UIKit

final class DateHeaderView: UICollectionReusableView {
    
    // MARK: - Propety list
    
    @IBOutlet private weak var titleLabel: UILabel!
    
    // MARK: - Public methods
    
    func setTitle(_ title: String?) {
        titleLabel.text = title
    }
}
