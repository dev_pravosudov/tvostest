//
//  CustomButton.swift
//  tvOSTZ
//
//  Created by Александр Правосудов on 02.08.2020.
//  Copyright © 2020 Александр Правосудов. All rights reserved.
//

import UIKit

final class CustomButton: UIButton, SelectionItemProtocol {
    
    // MARK: - SelectionItemProtocol
    
    func select() {
        backgroundColor = #colorLiteral(red: 0, green: 0.946221292, blue: 0.9373136163, alpha: 1)
    }
    
    func deselect() {
        backgroundColor = .clear
    }
}
