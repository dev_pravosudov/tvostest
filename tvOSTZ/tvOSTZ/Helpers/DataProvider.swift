//
//  DataProvider.swift
//  tvOSTZ
//
//  Created by Александр Правосудов on 01.08.2020.
//  Copyright © 2020 Александр Правосудов. All rights reserved.
//

import Foundation

final class DataProvider {
    
    // MARK: - Property list
    
    static let timePeriods: [String] = {
        let firstHour = "03:00 AM"
        let dateFormatter = DateFormatter.hhmma
        
        guard let firstDate = dateFormatter.date(from: firstHour) else { return [] }
        
        let halfHour: TimeInterval = 30 * 60
        let datesArray = Array(0..<50).map { firstDate.addingTimeInterval(halfHour * TimeInterval($0)) }
        
        return datesArray.map { dateFormatter.string(from: $0) }
    }()
    
    static let years: [String] = {
        let firstYear = "1900"
        let dateFormatter = DateFormatter.yyyy

        guard let firstDate = dateFormatter.date(from: firstYear) else { return [] }
        
        let oneYear: TimeInterval = 24 * 60 * 60 * 365
        let datesArray = Array(0..<122).map { firstDate.addingTimeInterval(oneYear * TimeInterval($0)) }
        
        return datesArray.map { dateFormatter.string(from: $0) }
    }()
    
    static let months = Calendar.current.monthSymbols
    
    static let days = Array(1...31).map { String($0) }
    
    static let dateHeaders = ["Year", "Month", "Day"]
}
