//
//  Typealiases.swift
//  tvOSTZ
//
//  Created by Александр Правосудов on 24.07.2020.
//  Copyright © 2020 Александр Правосудов. All rights reserved.
//

import Foundation

// MARK: - Api blocks

typealias ChannelsBlock = ([Channel]) -> Void
typealias ProgramsBlock = ([Program]) -> Void

// MARK: - Common

typealias DataBlock = (Data) -> Void
typealias ErrorBlock = (Error) -> Void
typealias EmptyBlock = () -> Void
