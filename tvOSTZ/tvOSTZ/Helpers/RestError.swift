//
//  RestError.swift
//  tvOSTZ
//
//  Created by Александр Правосудов on 24.07.2020.
//  Copyright © 2020 Александр Правосудов. All rights reserved.
//

import Foundation

enum RestError: LocalizedError {
    
    // MARK: - Cases
    
    case someRequestError
    case wrongURL(path: String)
    case decodingError
    
    // MARK: - Property list

    var errorDescription: String {
        switch self {
        case .someRequestError: return "Something on server goes wrong"
        case .wrongURL(let path): return "Wrong URL path: \(path)"
        case .decodingError: return "Some decoding error"
        }
    }
}
