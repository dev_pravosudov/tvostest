//
//  ApiRouter.swift
//  tvOSTZ
//
//  Created by Александр Правосудов on 27.07.2020.
//  Copyright © 2020 Александр Правосудов. All rights reserved.
//

import Foundation

enum ApiRouter {
    
    // MARK: - Cases
    
    case channels
    case programs
    
    // MARK: - Property list
    
    private var basePath: String { "https://demo-c.cdn.vmedia.ca/json/" }
    
    var path: String {
        switch self {
        case .channels: return basePath + "Channels"
        case .programs: return basePath + "ProgramItems"
        }
    }
}
