//
//  ApiManager+TV.swift
//  tvOSTZ
//
//  Created by Александр Правосудов on 27.07.2020.
//  Copyright © 2020 Александр Правосудов. All rights reserved.
//

protocol ApiManagerTV {
        
    func getChannels(success: @escaping ChannelsBlock, failure: @escaping ErrorBlock)
    func getPrograms(success: @escaping ProgramsBlock, failure: @escaping ErrorBlock)
}

extension ApiManager: ApiManagerTV {
    
    // MARK: - ApiManagerTV
    
    func getChannels(success: @escaping ChannelsBlock, failure: @escaping ErrorBlock) {
        getData(for: .channels, success: success, failure: failure)
    }
    
    func getPrograms(success: @escaping ProgramsBlock, failure: @escaping ErrorBlock) {
        getData(for: .programs, success: success, failure: failure)
    }
}
