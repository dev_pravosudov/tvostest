//
//  ApiManager.swift
//  tvOSTZ
//
//  Created by Александр Правосудов on 24.07.2020.
//  Copyright © 2020 Александр Правосудов. All rights reserved.
//

import Foundation

final class ApiManager {
    
    // MARK: - Property list
    
    static let shared = ApiManager()
    
    // MARK: - Initialization
    
    private init() { }
    
    // MARK: - Private methods
    
    private func getData(by urlPath: String, success: @escaping DataBlock, failure: @escaping ErrorBlock) {
        guard let url = URL(string: urlPath) else {
            failure(RestError.wrongURL(path: urlPath))
            return
        }
        
        URLSession.shared
            .dataTask(with: url, completionHandler: { data, response, error in
                if let error = error {
                    failure(error)
                    return
                }
                
                if let response = response as? HTTPURLResponse,
                    (200..<300).contains(response.statusCode),
                    let data = data {
                    success(data)
                } else {
                    failure(RestError.someRequestError)
                }
                
            }).resume()
    }
    
    // MARK: - Public methods
    
    func getData<T: Decodable>(for apiRouter: ApiRouter, success: @escaping (T) -> Void, failure: @escaping ErrorBlock) {
        getData(by: apiRouter.path, success: { data in
            DispatchQueue.main.async {
                if let genericSuccess = try? JSONDecoder.custom.decode(T.self, from: data) {
                    success(genericSuccess)
                } else {
                    failure(RestError.decodingError)
                }
            }
        }, failure: failure)
    }
}
