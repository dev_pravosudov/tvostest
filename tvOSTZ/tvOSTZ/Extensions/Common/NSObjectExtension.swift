//
//  NSObjectExtension.swift
//  tvOSTZ
//
//  Created by Александр Правосудов on 02.08.2020.
//  Copyright © 2020 Александр Правосудов. All rights reserved.
//

import Foundation

extension NSObject {
    
    /// Название класса в виде String
    static var className: String {
        return String(describing: self)
    }
}
