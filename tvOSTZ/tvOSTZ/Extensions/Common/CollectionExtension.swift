//
//  CollectionExtension.swift
//  tvOSTZ
//
//  Created by Александр Правосудов on 30.07.2020.
//  Copyright © 2020 Александр Правосудов. All rights reserved.
//

extension Collection {
    
    /// Возвращает элемент коллекции, если выйти за пределы коллекции, то вернет nil.
    subscript (safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
