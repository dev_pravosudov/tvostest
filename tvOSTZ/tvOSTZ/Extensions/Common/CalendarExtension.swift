//
//  CalendarExtension.swift
//  tvOSTZ
//
//  Created by Александр Правосудов on 01.08.2020.
//  Copyright © 2020 Александр Правосудов. All rights reserved.
//

import Foundation

extension Calendar {
    
    static let utcTimeZone: Calendar = {
        var calendar = Calendar.current
        calendar.timeZone = TimeZone(identifier: "UTC") ?? .current
        return calendar
    }()
}
