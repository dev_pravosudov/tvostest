//
//  DateExtension.swift
//  tvOSTZ
//
//  Created by Александр Правосудов on 01.08.2020.
//  Copyright © 2020 Александр Правосудов. All rights reserved.
//

import Foundation

extension Date {
    
    // MARK: - Property list
    
    var timeString: String { DateFormatter.hhmma.string(from: self) }
    var timeInMinutes: Int { (get(.hour) * 60) + get(.minute) }
    
    // MARK: - Public methods
    
    func get(_ type: Calendar.Component) -> Int {
        return Calendar.utcTimeZone.component(type, from: self)
    }
}
