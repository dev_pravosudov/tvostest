//
//  JSONDecoderExtension.swift
//  tvOSTZ
//
//  Created by Александр Правосудов on 27.07.2020.
//  Copyright © 2020 Александр Правосудов. All rights reserved.
//

import Foundation

extension JSONDecoder {
    
    static let custom: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(.yyyyMMDDTHHmmssZ)
        return decoder
    }()
}
