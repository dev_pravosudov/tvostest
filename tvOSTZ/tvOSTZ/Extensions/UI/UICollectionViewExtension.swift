//
//  UICollectionViewExtension.swift
//  tvOSTZ
//
//  Created by Александр Правосудов on 02.08.2020.
//  Copyright © 2020 Александр Правосудов. All rights reserved.
//

import UIKit

extension UICollectionView {

    func registerWithNib<T: UICollectionViewCell>(cellType: T.Type, bundle: Bundle? = nil) {
        let nib = UINib(nibName: cellType.className, bundle: bundle)
        register(nib, forCellWithReuseIdentifier: cellType.className)
    }
    
    func registerWithNib<T: UICollectionReusableView>(viewType: T.Type, bundle: Bundle? = nil) {
        let nib = UINib(nibName: viewType.className, bundle: bundle)
        register(nib, forSupplementaryViewOfKind: viewType.className, withReuseIdentifier: viewType.className)
    }
        
    func dequeue<T: UICollectionViewCell>(cell: T.Type, for indexPath: IndexPath) -> T {
        guard let reusableCell = dequeueReusableCell(withReuseIdentifier: cell.className,
                                                     for: indexPath) as? T else {
            fatalError("Can't reuse cell: \(cell.className)")
        }
        return reusableCell
    }
    
    func dequeue<T: UICollectionReusableView>(view: T.Type, for indexPath: IndexPath) -> T {
        guard let reusableView = dequeueReusableSupplementaryView(ofKind: view.className,
                                                                  withReuseIdentifier: view.className,
                                                                  for: indexPath) as? T else {
            fatalError("Can't reuse supplementary view: \(view.className)")
        }
        return reusableView
    }
}

