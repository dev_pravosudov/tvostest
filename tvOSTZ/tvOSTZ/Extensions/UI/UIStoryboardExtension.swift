//
//  UIStoryboardExtension.swift
//  tvOSTZ
//
//  Created by Александр Правосудов on 03.08.2020.
//  Copyright © 2020 Александр Правосудов. All rights reserved.
//

import UIKit

extension UIStoryboard {
    
    /// Простой способ создать вью контроллер из сториборда. Важно чтобы название вью контроллера,
    /// сториборда (в котором он лежит) и id вью контроллера в этом сториборде были одинаковыми.
    /// - Parameter screenType: Тип вью контроллера который нужно создать из сториборда.
    static func screen<T: UIViewController>(_ screenType: T.Type) -> T {
        let screenName = screenType.className
        let story = UIStoryboard(name: screenName, bundle: nil)
       
        guard let vc = story.instantiateViewController(withIdentifier: screenName) as? T else {
            fatalError("No ViewController in \(screenName).storyboard with id: \(screenName)")
        }
        
        return vc
    }
}
