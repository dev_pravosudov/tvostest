//
//  UIViewExtension.swift
//  tvOSTZ
//
//  Created by Александр Правосудов on 02.08.2020.
//  Copyright © 2020 Александр Правосудов. All rights reserved.
//

import UIKit

extension UIView {
    
    /// Упрощенный способ установить isHidden = true
    func hide() {
        isHidden = true
    }
    
    /// Упрощенный способ установить isHidden = false
    func show() {
        isHidden = false
    }
}
